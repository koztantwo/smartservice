package com.example.smartservice;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by sebas on 14.10.2016.
 */

public class ItemAdapter2 extends ArrayAdapter<Item> {

    Context context;
    int layoutResourceId;
    Item data[] = null;

    public ItemAdapter2(Context context, int layoutResourceId, Item[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        System.out.println("test2");
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        WeatherHolder holder = null;

        final Item weather = data[position];

        System.out.println("test");

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new WeatherHolder();
            holder.aSwitch = (Switch)row.findViewById(R.id.switch1);

            row.setTag(holder);
            holder.aSwitch.setChecked(weather.bool);
            final WeatherHolder finalHolder = holder;
            //holder.aSwitch.setOnClickListener(new View.OnClickListener() {
              //  @Override
                //public void onClick(View view) {
                  //  weather.bool = finalHolder.aSwitch.isChecked();
                    //System.out.println(position+" clicked.");
                //}
            //});
            holder.aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    weather.bool = finalHolder.aSwitch.isChecked();
                    System.out.println(position+" clicked.");
                }
            });
        }
        else
        {
            holder = (WeatherHolder)row.getTag();
                //holder.aSwitch.setChecked(weather.bool);
                System.out.println(position+" set to "+holder.aSwitch.isChecked());
                weather.bool = holder.aSwitch.isChecked();

        }

        holder.aSwitch.setText(weather.string);

        return row;
    }

    static class WeatherHolder
    {
        Switch aSwitch;
    }
}
