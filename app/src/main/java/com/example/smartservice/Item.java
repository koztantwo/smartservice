package com.example.smartservice;

import android.widget.Switch;

/**
 * Created by sebas on 14.10.2016.
 */

public class Item {

    public boolean bool;
    public String string;

    public Item(String string,boolean bool)
    {
        super();
        this.bool = bool;
        this.string = string;
    }
}
