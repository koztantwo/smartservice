package com.example.smartservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.sql.SQLOutput;
import java.util.Arrays;

/**
 * Created by sebas on 14.10.2016.
 */

public class Toggles extends AppCompatActivity {

    private ListView listview;

    private ItemAdapter2 itemAdapter;

    private Button b;

    Item[] items = new Item[]{
            new Item("P1111-Ölkühlung", false),
            new Item("P1112-Getriebe", false),
            new Item("P1201-Kühlung", false),
            new Item("P1209-Motor", false),
            new Item("P1225-Gaspedal", false),
            new Item("P1237-Einspritzung", false),
            new Item("P1257-Abgas", false),
            new Item("P1296-Fahrwerk", false),
            new Item("P1287-Reifen", false),
            new Item("P1335-Querlenker", false),
            new Item("P1341-Lichtmaschine", false)


             /*
            P1111-OelKuehlung",
        "P1112-Getriebe",
        "P1201-Kuehlung",
        "P1209-Motor",
        "P1225-Gaspedal",
        "P1237-Einspritzung",
        "P1257-Abgas",
        "P1296-Fahrwerk",
        "P1287-Reifen",
        "P1335-Querlenker",
        "P1341-Lichtmaschine",
        "Classification"
             */
    };

    /**
     * Initializes the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toggleview);

        Intent intent = getIntent();




        //myToolbar.setTitle("OBD Fehlercodes");
        b = (Button)findViewById(R.id.button);


        itemAdapter = new ItemAdapter2(this,
                R.layout.toggable_listviewitem, items);


        listview = (ListView)findViewById(R.id.listView1);

        listview.setAdapter(itemAdapter);

    }

    private boolean[] getData()
    {

        boolean[] arr = new boolean[items.length];
        for (int i = 0; i < items.length; i++) {
            arr[i] = items[i].bool;
        }
        return arr;
    }

    public void click5(View view){
        boolean[] data = getData();
        System.out.println(Arrays.toString(data));
        Intent intent = new Intent(this, Result.class);
        intent.putExtra("Toggels", data);
        startActivity(intent);
    }

}
