package com.example.smartservice;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.Arrays;

public class ToDoActivity extends AppCompatActivity {

    /**
     * Initializes the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);
    }

    public void click(View view)
    {
        Intent intent = new Intent(this, Toggles.class);
        startActivity(intent);
    }
    public void click2(View view)
    {
        Intent intent = new Intent(this, Result.class);
        startActivity(intent);
    }
}