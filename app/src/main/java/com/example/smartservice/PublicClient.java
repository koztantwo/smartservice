package com.example.smartservice;

import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.net.URLConnection;

/**
 * Created by sebas on 14.10.2016.
 */

public class PublicClient {

    public static void rrsHttpPost(boolean[] arr) {

        HttpPost post;
        HttpClient client;
        StringEntity entity;

        String mlEndPoint = "https://europewest.services.azureml.net/workspaces/49f83800794249a1b4c39765374ac115/services/64009aaa074742e0a857c89cad6adeed/execute?api-version=2.0&details=true";
        String mlAPIKey="/mtD2SFMy9/AgrCiFmYmNF95eDl4ZTKsROk4IfVlKmouGceVCpNibO3cm/VIEx2fcAM4ydwTrc+BgoD7Ulr+aA==";
        String requestBody = parseJson(arr);

        AzureMLClient myMLClient = new AzureMLClient(mlEndPoint,mlAPIKey, null, true);
            myMLClient.execute(requestBody);
        try {

            //System.out.println(response);
            //return response;
        } catch (Exception e) {
            e.printStackTrace();
            //return null;
        }
    }

    public static void changeLabel(boolean[] arr, TextView t)
    {
        HttpPost post;
        HttpClient client;
        StringEntity entity;

        String mlEndPoint = "https://europewest.services.azureml.net/workspaces/49f83800794249a1b4c39765374ac115/services/64009aaa074742e0a857c89cad6adeed/execute?api-version=2.0&details=true";
        String mlAPIKey="/mtD2SFMy9/AgrCiFmYmNF95eDl4ZTKsROk4IfVlKmouGceVCpNibO3cm/VIEx2fcAM4ydwTrc+BgoD7Ulr+aA==";
        String requestBody = parseJson(arr);

        AzureMLClient myMLClient = new AzureMLClient(mlEndPoint,mlAPIKey, t, false);
        myMLClient.execute(requestBody);
        try {

            //System.out.println(response);
            //return response;
        } catch (Exception e) {
            e.printStackTrace();
            //return null;
        }
    }

    public static String parseJson(boolean[] arr){

        String ret = "{\"Inputs\": { \"input1\": {\"ColumnNames\": [ \"P1111-OelKuehlung\", \"P1112-Getriebe\", \"P1201-Kuehlung\", \"P1209-Motor\", \"P1225-Gaspedal\", \"P1237-Einspritzung\", \"P1257-Abgas\", \"P1296-Fahrwerk\", \"P1287-Reifen\", \"P1335-Querlenker\", \"P1341-Lichtmaschine\", \"Classification\"],\"Values\": [ [ ";
        for (int i = 0; i < arr.length; i++) {
            ret+=arr[i]?"\"1\"":"\"0\"";
            ret+=", ";
        }
        ret+="\"value\" ], [ ";
        for (int i = 0; i < arr.length; i++) {
            ret+=arr[i]?"\"1\"":"\"0\"";
            ret+=", ";
        }
        ret += "\"value\"]]}},\"GlobalParameters\": {}}";

        return ret;
    }
}
