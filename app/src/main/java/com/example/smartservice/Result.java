package com.example.smartservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by sebas on 14.10.2016.
 */

public class Result extends Activity {

    boolean data [];
    String names [] = new String[]{
            "P1111-OelKuehlung",
            "P1112-Getriebe",
            "P1201-Kuehlung",
            "P1209-Motor",
            "P1225-Gaspedal",
            "P1237-Einspritzung",
            "P1257-Abgas",
            "P1296-Fahrwerk",
            "P1287-Reifen",
            "P1335-Querlenker",
            "P1341-Lichtmaschine"
    };
    ArrayList<String> listItems=new ArrayList<String>(){
    };

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        Intent intent = getIntent();
        if (intent != null){
            data = intent.getBooleanArrayExtra("Toggels");
            if(data != null) {
                for (int i = 0; i < data.length; i++) {
                    if (data[i]) {
                        listItems.add(names[i]);
                    }
                }
            }
            else
            {
                listItems.add("P1111 - Ölkühlung");
                listItems.add("P1201 - Kühlung");
                listItems.add("P1209 - Motor");

                data = getData();
            }
        }
        else {
            listItems.add("P1111 - Ölkühlung");
            listItems.add("P1201 - Kühlung");
            listItems.add("P1209 - Motor");

            data = getData();
        }



        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        ((ListView)findViewById(R.id.listview2)).setAdapter(adapter);

        Button b = (Button)findViewById(R.id.button2);
    }

    private boolean[] getData()
    {
        boolean[] arr = new boolean[]{
            true,
                false,
                true,
                true,
                false,
                false,
                false,
                false,
                false,
                false,
                false
        };
        return arr;
    }
    public void click(View view)
    {
        TextView t = (TextView)findViewById(R.id.textView3) ;
        try {
            //System.out.println("Response:\n" + PublicClient.rrsHttpPost(data));
            System.out.println("Response: \n");
            PublicClient.changeLabel(data, t);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void click2(View view)
    {
        Intent intent = new Intent(this, Toggles.class);
        startActivity(intent);
    }
}
