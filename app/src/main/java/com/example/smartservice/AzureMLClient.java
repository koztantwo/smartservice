package com.example.smartservice;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import java.net.*;
import java.io.*;
import java.util.*;

public class AzureMLClient extends AsyncTask<String, Void, String>
{
    private Exception exception;

      private String endPointURL; //Azure ML Endpoint
      private String key; //API KEY
        private  TextView t;
    private boolean isToggleCall;

      public AzureMLClient(String endPointURL, String key, TextView t, boolean isToogleCall)
      {
        this.endPointURL= endPointURL;
        this.key= key;
          this.t = t;
          this.isToggleCall = isToogleCall;
      }
      /*
       Takes an Azure ML Request Body then Returns the Response String Which Contains Scored Lables etc
      */
      public  String requestResponse( String requestBody ) throws Exception
      {
        URL u = new URL(this.endPointURL);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();

        conn.setRequestProperty("Authorization","Bearer "+ this.key);
        conn.setRequestProperty("Content-Type","application/json");
        conn.setRequestMethod("POST");
        
        String body= new String(requestBody);
        
        conn.setDoOutput(true);
        OutputStreamWriter wr=new OutputStreamWriter(conn.getOutputStream());

        wr.write(body);
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        
        String decodedString;
        String responseString="";    

        while ((decodedString = in.readLine()) != null) 
    	  {
            responseString+=decodedString;
        }
    	  return responseString;
     }

    @Override
    protected String doInBackground(String[] strings) {

        try {
            return this.requestResponse(strings[0]);
        } catch (Exception e) {
            e.printStackTrace();
            this.exception = e;

            return null;
        }
    }

    protected void onPostExecute(String feed) {
        System.out.println(feed);
        if(!isToggleCall)
        {
            String[] output = feed.split("\"");
            t.setText(output[output.length-2]);
            t.setVisibility(View.VISIBLE);
        }
        else{

        }
    }
}
